Choropleth and equal-area square cartogram of Chinese population (older than 16) active in service sector.

Inspired by Maarten Lambrecht's [Europe example](http://www.maartenlambrechts.com/2017/05/03/the-eurosearch-song-contest-making-of.html), using [geo2rect](https://github.com/sebastian-meier/d3.geo2rect) by Sebastian Meier. Geojson data for China map is from [china-geojson](https://github.com/antvis/china-geojson).

Color choice via [ColorBrewer](http://colorbrewer2.org/#type=sequential&scheme=Greens&n=6) (6 sequencial green).

forked from <a href='http://bl.ocks.org/officeofjane/'>officeofjane</a>'s block: <a href='http://bl.ocks.org/officeofjane/d33d6ef783993b60b15a3fe0f8da1481'>China tile grid map</a>